<?php

namespace App\Controller;

use App\Services\Client\City\CityChecker;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{

    #[Route('/', name: 'Home')]
    public function home(): Response
    {
        return $this->render('home.html.twig');
    }
    #[Route('/check', name: 'check', methods: 'POST')]
    public function checkCity(Request $request, CityChecker $checker): JsonResponse
    {
        $playerChoice = json_decode($request->getContent(), true);
        $diff = $checker->CheckCity($playerChoice['city'], $playerChoice['cityCode']);
        return new JsonResponse($diff, 200);
    }
}