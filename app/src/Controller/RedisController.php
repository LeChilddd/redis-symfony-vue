<?php

namespace App\Controller;


use App\Services\Client\City\ApiCityService;
use Psr\Cache\InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class RedisController extends AbstractController
{

    /**
     * @param ApiCityService $api
     * @param string $chars
     * @param int $code
     * @return JsonResponse
     * @throws TransportExceptionInterface
     * @Route("/api/city/{chars}/{code}", name="api_city", methods={"GET"})
     */
    public function getCity(ApiCityService $api, string $chars, int $code): JsonResponse
    {
        return $this->json([
            'city' => $api->getCity($chars, $code)
        ]);
    }

}