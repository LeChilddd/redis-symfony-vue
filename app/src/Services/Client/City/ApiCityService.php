<?php

namespace App\Services\Client\City;

use App\Services\DTO\CityDTO;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ApiCityService
{
    private const END_POINT = 'https://geo.api.gouv.fr/communes';
    private const BOOST = 'population';
    private const LIMIT = 1;
    public function __construct(
        private readonly HttpClientInterface $client
    )
    {
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function getCity(string $name, int $departmentCode): CityDTO
    {
        $data = $this->client->request(
            'GET',
            self::END_POINT,
            ['query' => [
                'nom' => $name,
                'departement' => $departmentCode,
                'boost' => self::BOOST,
                'limit' => self::LIMIT,
                'fields' => "code,nom,departement,region,centre,population"
            ]]
        );
        try {
            $data = json_decode(
                $data->getContent(),
                true,
                512,
                JSON_THROW_ON_ERROR
            );
        } catch (\JsonException
        |ClientExceptionInterface
        |RedirectionExceptionInterface
        |ServerExceptionInterface
        |TransportExceptionInterface $e) {
            dd($e);
        };
        return CityDTO::fromArrayToObject($data[0]);
    }
}