<?php

namespace App\Services\Client\City;

use App\Services\Client\ClientTrait;
use App\Services\Client\JsonParserTrait;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

trait CityClientTrait
{
    use JsonParserTrait;
    use ClientTrait;

    private const BOOST = 'population';
    private const LIMIT = 1;
    private const FIELDS = "code,nom,departement,region,centre,population";


    public function __construct(
        private readonly HttpClientInterface   $client,
        private readonly LoggerInterface       $logger,
        private readonly ParameterBagInterface $parameterBag,
        protected readonly CacheInterface      $cache,
    )
    {
    }

    final protected function getBaseUrl(): string
    {
        return "https://geo.api.gouv.fr/communes";
    }

}
