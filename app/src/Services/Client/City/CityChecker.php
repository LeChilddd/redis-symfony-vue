<?php

namespace App\Services\Client\City;

use App\Services\DTO\CityDTO;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Contracts\Cache\ItemInterface;

class CityChecker extends CityFinder
{
    final public function CheckCity(string $city, int $departmentCode):array
    {
        $playerCity = $this->find($city, $departmentCode);
        $cityToGuess = $this->getCityToGuess();

        if($playerCity->getRegion() === $cityToGuess->getRegion()){
            $diff['region'] = true;
        }else{
            $diff['region'] = false;
        }

        if($playerCity->getDepartment()['code'] === $cityToGuess->getDepartment()['code']){
            $diff['departmentCode'] = true;
        }elseif ($playerCity->getDepartment()['code'] > $cityToGuess->getDepartment()['code']){
            $diff['departmentCode'] = 'too high';
        }else{
            $diff['departmentCode'] = 'too low';
        }

        if($playerCity->getPopulation() === $cityToGuess->getPopulation()){
            $diff['population'] = true;
        }elseif ($playerCity->getPopulation() > $cityToGuess->getPopulation()){
            $diff['population'] = 'too high';
        }else{
            $diff['population'] = 'too low';
        }

        if($playerCity->getCoordinates()[1] === $cityToGuess->getCoordinates()[1]){
            $diff['latitude'] = true;
        }elseif ($playerCity->getCoordinates()[1] > $cityToGuess->getCoordinates()[1]){
            $diff['latitude'] = 'too high';
        }else{
            $diff['latitude'] = 'too low';
        }

        if($playerCity->getCoordinates()[0] === $cityToGuess->getCoordinates()[0]){
            $diff['longitude'] = true;
        }elseif ($playerCity->getCoordinates()[0] > $cityToGuess->getCoordinates()[0]){
            $diff['longitude'] = 'too high';
        }else{
            $diff['longitude'] = 'too low';
        }

        return ['city' => $playerCity->ObjectToArray(), 'diff' => $diff];

    }

    private function getCityToGuess(){
        return $this->find("Vesoul", "70000");
    }


}
