<?php

namespace App\Services\Client\City;

use App\Services\DTO\CityDTO;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Cache\ItemInterface;

class CityFinder
{
    use CityClientTrait;

    /**
     * @throws InvalidArgumentException
     */
    final public function find(string $city, int $department): CityDTO
    {
        return $this->cache->get("city_{$city}_department_{$department}",
            function (ItemInterface $item) use ($city, $department) {
                $content = CityDTO::fromArrayToObject($this->callGetCity($city, $department));
                $item->expiresAfter($this->getCacheExpirationTime());
                return $content;
            });
    }

    final protected function callGetCity(string $city, int $department): array
    {
        $url = $this->getBaseUrl();
        $response = $this->client->request(
            Request::METHOD_GET,
            $url,
            ['query' => [
                'nom' => $city,
                'departement' => $department,
                'boost' => self::BOOST,
                'fields' => "code,nom,departement,region,centre,population"
            ]]
        );
        return $this->parseJsonResponse($response)[0];
    }

}
