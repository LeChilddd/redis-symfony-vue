<?php

namespace App\Services\Client;

use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

trait JsonParserTrait
{
    private readonly LoggerInterface $logger;

    final public function parseJsonResponse(ResponseInterface $response): array
    {
        try {
            $result = json_decode(
                $response->getContent(),
                true,
                512,
                JSON_THROW_ON_ERROR
            );
        } catch (\Throwable $throwable) {
            $this->logger->critical(__CLASS__ . " response is not JSON type");
            $this->logger->critical($throwable->getMessage());
            $this->logger->critical($throwable->getTraceAsString());
        }

        return $result ?? [];
    }
}

