<?php

namespace App\Services\Client;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

trait ClientTrait
{
    private readonly ParameterBagInterface $parameterBag;

    final protected function getCacheExpirationTime(): int
    {
        return 3600;
    }

}
