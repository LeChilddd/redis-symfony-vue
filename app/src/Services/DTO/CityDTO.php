<?php

namespace App\Services\DTO;

use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class CityDTO
{
    final public function __construct(
        private int $id,
        private string $name,
        private array $department,
        private int $population,
        private string $region,
        private array $coordinates,
    )
    {
    }

    final public static function fromArrayToObject($data): CityDTO
    {
        return new self(
            $data['code'],
            $data['nom'],
            $data['departement'],
            $data['population'],
            $data['region']['nom'],
            $data['centre']['coordinates'],
        );
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getRegion(): string
    {
        return $this->region;
    }

    public function setRegion(string $region): void
    {
        $this->region = $region;
    }

    public function getDepartment(): array
    {
        return $this->department;
    }

    public function setDepartment(array $department): void
    {
        $this->department = $department;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getPopulation(): int
    {
        return $this->population;
    }

    public function setPopulation(int $population): void
    {
        $this->population = $population;
    }

    public function getCoordinates(): array
    {
        return $this->coordinates;
    }

    public function setCoordinates(array $coordinates): void
    {
        $this->coordinates = $coordinates;
    }

    public function ObjectToArray(): array
    {
        $normalizer = new Serializer([new ObjectNormalizer()]);
        return $normalizer->normalize($this, null);
    }

}