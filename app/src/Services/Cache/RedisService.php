<?php

namespace App\Services\Cache;

use App\Services\Api\ApiPopulationService;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;

Class RedisService {

    public function __construct(
        private readonly CacheInterface $cache,
        private readonly ApiPopulationService $apiPopulation,
    )
    {
    }

    /**
     * @param String $key
     * @return JsonResponse
     * @throws InvalidArgumentException
     */
    public function get(String $key): JsonResponse
    {
        try {
            $el = $this->cache->get($key,
                function (ItemInterface $item) use ($key) {
                    if (empty($data)) {
                        $data = $this->apiPopulation->getPopulation($key);
                    }
                    $item->expiresAfter(3600);
                    return $data;
                });
        } catch (\Exception $exception){
            dd($exception);
        }

        return $el;

    }
}