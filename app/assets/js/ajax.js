import axios from "axios";

export const getCities = async (string) => {

    return await axios.get('https://geo.api.gouv.fr/communes', {
        params: {
            nom: string,
            boost: "population",
            limit: 5,
            fields: "code,nom,departement"
        }
    }).then(res => {
        return res.data
    })
        .catch(err => console.log(err))
};
