import App from '@assets/js/vue/components/App.vue'
import { createApp } from 'vue'

const app = createApp(App);
app.mount('#app')